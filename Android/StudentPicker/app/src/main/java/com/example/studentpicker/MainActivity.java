package com.example.studentpicker;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;

import android.content.res.Resources;
import android.os.Bundle;

import android.view.View;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    public String[] studentnames;
    public String[] usedstudents = new String[0];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickSelectStudent(View view) {
        TextView student = (TextView) findViewById(R.id.student);

        Resources res = getResources();
        studentnames = res.getStringArray(R.array.students);

        int n = studentnames.length;
        int m = usedstudents.length;

        if (m >= n) {
            //if all students are picked
            usedstudents = new String[0];
            //clear usedstudents
        }


        int choice = 0;
        Random r = new Random();
        boolean satisfied = false;
        while (!satisfied) {
            int random = r.nextInt(n);
            boolean canuse = true;
            for (String element : usedstudents) {
                if (element == studentnames[random]) {
                    canuse = false;
                    break;
                }
            }
            if (canuse) {
                choice = random;
                satisfied = true;
            }
        }

        student.setText(studentnames[choice]);
    }
}